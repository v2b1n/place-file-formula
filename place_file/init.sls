# Place a file
# source can be either a file or folder in salt:// of any module or a contents of a pillar (for a single file)

{%- for file,values in salt['pillar.get']('place_file:files', {}).items() %}

{% set file_dest = values['file_dest'] %}


{% if values['type'] in ['file','folder'] %}
{{ file_dest }}:
{% if values['type'] in "folder" %}
  file.recurse:
{% else %}
  file.managed:
{% endif %}
    {% if values['file_src'] is defined %}
    {% set file_src = values['file_src'] %}
    - source: {{ file_src }}
    {% else %}
    {% set file_content = values['file_content'] %}
    - contents_pillar: place_file:files:{{ file }}:file_content
    {% endif %}
    {% if values['file_user'] is defined %}
    {% set file_user = values['file_user'] %}
    - user: {{ file_user }}
    {% endif %}
    {% if values['file_group'] is defined %}
    {% set file_group = values['file_group'] %}
    - group: {{ file_group }}
    {% endif %}
    {% if values['type'] in "folder" %}
    - recurse:
      - user
      - group
      - mode
    {% if values['file_mode'] is defined %}
    {% set file_mode = values['file_mode'] %}
    - file_mode: {{ file_mode }}
    {% endif %}
    {% if values['dir_mode'] is defined %}
    {% set dir_mode = values['dir_mode'] %}
    - dir_mode: {{ dir_mode }}
    {% endif %}
    {% else %}
    {% if values['file_mode'] is defined %}
    {% set file_mode = values['file_mode'] %}
    - mode: {{ file_mode }}
    {% endif %}
    {% endif %}
    - makedirs: True
    - template: jinja

{% endif %} # EO file,folder


{% if values['type'] in ['archive'] %}
{% set file_src = values['file_src'] %}
{{ file_dest }}:
  archive.extracted:
    - name: {{ file_dest }}
    - source: {{ file_src }}
    {% if values['source_hash'] is defined %}
    {% set source_hash = values['source_hash'] %}
    - source_hash: {{ source_hash }}
    {% else %}
    - skip_verify: True
    {% endif %}
    {% set if_missing = values['if_missing'] %}
    - if_missing: {{ if_missing }}
    {% if values['file_user'] is defined %}
    {% set file_user = values['file_user'] %}
    - user: {{ file_user }}
    {% endif %}
    {% if values['file_group'] is defined %}
    {% set file_group = values['file_group'] %}
    - group: {{ file_group }}
    {% endif %}

{% endif %} # EO archive


{% if values['type'] in ['git'] %}
{% set file_src = values['file_src'] %}
{{ file_dest }}:
  git.latest:
    - name: {{ file_src }}
    # https_user
    {% if values['https_user'] is defined %}
    {% set https_user = values['https_user'] %}
    - https_user: {{ https_user }}
    {% endif %} 
    # https_pass
    {% if values['https_pass'] is defined %}
    {% set https_pass = values['https_pass'] %}
    - https_pass: {{ https_pass }}
    {% endif %} 
    - target: {{ file_dest }}
    # branch
    {% if values['branch'] is defined %}
    {% set branch = values['branch'] %}
    - branch: {{ branch }}
    {% endif %} 
    # rev
    {% if values['rev'] is defined %}
    {% set rev = values['rev'] %}
    - rev: {{ rev }}
    {% endif %} 
    # force_reset
    {% if values['force_reset'] is defined %}
    {% set force_reset = values['force_reset'] %}
    - force_reset: {{ force_reset }}
    {% endif %} 
    # user
    # User under which to run git commands. By default, commands are run by the user under which the minion is running.
    {% if values['user'] is defined %}
    {% set user = values['user'] %}
    - user: {{ user }}
    {% endif %} 
    # identity
    {% if values['identity'] is defined %}
    {% set identity = values['identity'] %}
    - identity: {{ identity }}
    {% endif %} 

{% endif %} # EO git


# setting permissions for archives or checked out repos
{% if values['type'] in ["git","archive"] %}
{% if ( values['file_user'] is defined ) or (values['file_group'] is defined ) or  ( values['file_mode'] is defined ) or (values['dir_mode'] is defined ) %}

ensure_permissions_for_{{ file_dest }}:
  file.directory:
    - name: {{ file_dest }}

    {% if values['file_user'] is defined %}
    {% set file_user = values['file_user'] %}
    - user: {{ file_user }}
    {% endif %}

    {% if values['file_group'] is defined %}
    {% set file_group = values['file_group'] %}
    - group: {{ file_group }}
    {% endif %}

    {% if values['file_mode'] is defined %}
    {% set file_mode = values['file_mode'] %}
    - file_mode: {{ file_mode }}
    {% endif %}

    {% if values['dir_mode'] is defined %}
    {% set dir_mode = values['dir_mode'] %}
    - dir_mode: {{ dir_mode }}
    {% endif %}

    - recurse:
      {% if values['file_user'] is defined %}
      - user
      {% endif %}
      {% if values['file_group'] is defined %}
      - group
      {% endif %}
      {% if ( values['dir_mode'] is defined ) or (values['file_mode'] is defined ) %}
      - mode
      {% endif %}
      

    - watch:
      {% if values['type'] in "git" %}
      - git: {{ file_dest }}
      {% endif %}
      {% if values['type'] in "archive" %}
      - archive: {{ file_dest }}
      {% endif %}

{% endif %} # EO ["git","archive"] differentiation
{% endif %} # EO permissions


{% endfor %}
