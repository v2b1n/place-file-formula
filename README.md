# Place file

Formula to place to a salt-minion a

* single file from salt://
* single file using a content of a pillar
* folder from salt://
* remote archive and unpack it to a local folder
* remote git repository served via ssh/http/https to a local destination

and set owner & permissions.


# Example

See place_file/pillar.example
